#!/bin/bash -x

#set -e -u

# PROFILE
echo 'PATH=$PATH:/root/bin' >> /root/.bashrc

# package builduser
if (!  id "builduser" >/dev/null 2>&1; ) then
	useradd -g wheel builduser
	mkdir -p /home/builduser 
	chown builduser /home/builduser
fi
sed -i 's/# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers

for pkg in `cat /root/aurpackages.list`; do
	su builduser -c "cd /opt/aurpackages/${pkg}; makepkg -icf --noconfirm" 
done
